import pixiManager from "graphics/pixi-manager";
import { Graphics } from "pixi.js";
import { Vector } from "util/vector";

export class Ball {

    public readonly position: Vector;
    public readonly radius: number;
    public readonly color: number;

    private velocity: Vector;

    private lifetime: number;
    private target: Vector;

    private MIN_SPEED: number = 10;
    private MAX_SPEED: number = 30;
    private LIFETIME: number = 1000;
    private RADIUS: number = 50;
    private TOLERANCE: number = 50;

    constructor() {

        const speed = this.MIN_SPEED + Math.random() * (this.MAX_SPEED - this.MIN_SPEED);

        this.lifetime = this.LIFETIME;

        this.radius = this.RADIUS;

        const direction = Math.random() * (Math.PI * 2);

        this.position = {
            x: this.radius + Math.random() * (pixiManager.canvas.width - this.radius * 2),
            y: this.radius + Math.random() * (pixiManager.canvas.height - this.radius * 2)
        }

        this.velocity = {
            x: Math.cos(direction) * speed,
            y: Math.sin(direction) * speed
        }

        const r = Math.round(Math.random() * 0xff);
        const b = Math.round(Math.random() * 0xff);
        this.color = (r << 16) | b;

        this.target = {
            x: (r / 0xff) * pixiManager.canvas.width,
            y: (b / 0xff) * pixiManager.canvas.height
        };
    }

    public update() {

        if (this.position.x <= this.radius || this.position.x >= (pixiManager.canvas.width - this.radius)) {
            this.velocity.x = -this.velocity.x;
        }

        if (this.position.y <= this.radius || this.position.y >= (pixiManager.canvas.height - this.radius)) {
            this.velocity.y = -this.velocity.y;
        }


        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;

        this.lifetime--;
    }

    public draw(graphics: Graphics) {
        graphics.beginFill(this.color);
        graphics.drawCircle(this.position.x, this.position.y, this.radius);
        graphics.endFill();
    }

    public isTargetReached() {
        const a = this.position.x - this.target.x;
        const b = this.position.y - this.target.y;

        const c = a * a + b * b;

        return c < (this.TOLERANCE * this.TOLERANCE);
    }

    public isLifetimeOver() {
        return this.lifetime <= 0;
    }
}