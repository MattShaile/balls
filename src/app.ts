import "pixi.js";
import pixiManager from "graphics/pixi-manager";
import { BallController } from "ball-controller";

const ballController = new BallController();

const tick = () => {
    ballController.update();

    requestAnimationFrame(tick);
};

tick();