import { Ball } from "ball";
import pixiManager from "graphics/pixi-manager";
import { Graphics, Texture } from "pixi.js";

export class BallController {

    private movingBalls: Graphics;
    private staticBalls: Graphics;

    private balls: Ball[];

    constructor() {
        this.balls = [];

        this.movingBalls = new Graphics();
        this.staticBalls = new Graphics();

        pixiManager.stage.addChild(this.movingBalls);
        pixiManager.stage.addChild(this.staticBalls);

        setInterval(() => this.spawnBall(1), 10);
    }

    public spawnBall(count: number) {
        for (let i = 0; i < count; i++) {
            const ball = new Ball()
            this.balls.push(ball);
        }
    }

    public update() {
        this.balls.forEach((ball) => {
            ball.update();
        });

        this.draw();
    }

    protected draw() {
        this.movingBalls.clear();

        const deadBallIndexes: number[] = [];

        this.balls.forEach((ball, index) => {
            if (ball.isTargetReached()) {
                ball.draw(this.staticBalls);
                deadBallIndexes.push(index);
            } else if (ball.isLifetimeOver()) {
                deadBallIndexes.push(index);
            } else {
                ball.draw(this.movingBalls);
            }
        });

        deadBallIndexes.reverse().forEach((index) => {
            this.balls.splice(index, 1);
        });
    }

}